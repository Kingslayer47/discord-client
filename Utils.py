import json
import uuid
import random
import string
import aiohttp


class Embed():
    def __init__(self, **kwargs):
        embed_structure = {}
        embed_structure['title'] = kwargs.get('title',None)
        embed_structure['type'] = kwargs.get('type','rich')
        embed_structure['description'] = kwargs.get('description', None)
        embed_structure['url'] = kwargs.get('url', None)
        embed_structure['timestamp'] = kwargs.get('timestamp', None)
        embed_structure['color'] = kwargs.get('color',0xffffff)
        embed_structure['fields'] = []
        self.video = False
        self.embed_json = embed_structure
        self.session = aiohttp.ClientSession()


    def set_footer(self,text,icon_url=None,proxy_icon_url=None):
        footer_structure = {}
        if text:
            footer_structure['text'] = text
        
        if icon_url:
            footer_structure['icon_url'] = icon_url
        
        if proxy_icon_url:
            footer_structure['proxy_icon_url'] = proxy_icon_url

        self.embed_json['footer'] = footer_structure

    def set_image(self, url, proxy_url=None, height=None, width=None):
        image_structure = {}

        if url:
            image_structure['url'] = url

        if proxy_url:
            image_structure['proxy_url'] = proxy_url

        if height:
            image_structure['height'] = height

        if width:
            image_structure['width'] = width

        self.embed_json['image'] = image_structure
    
    def set_thumbnail(self, url, proxy_url=None, height=None, width=None):
        thumbnail_structure = {}
        if url:
            thumbnail_structure['url'] = url

        if proxy_url:
            thumbnail_structure['proxy_url'] = proxy_url

        if height:
            thumbnail_structure['height'] = height

        if width:
            thumbnail_structure['width'] = width

        self.embed_json['thumbnail'] = thumbnail_structure

    async def add_video(self, bot , url, title='', color_hex=''):
        apiUrl = "https://data.mongodb-api.com/app/data-ujpjf/endpoint/data/beta/action/insertOne"
        payload = json.dumps({
            "collection": "ogpVideo",
            "database": "ogpData",
            "dataSource": "Cluster0",
            "document": {
                "url": url,
                "title":title,
                "color":color_hex
            }
        })
        headers = {
            'Content-Type': 'application/json',
            'Access-Control-Request-Headers': '*',
            'api-key': ''
        }
        
        async with self.session.post(url=apiUrl,headers=headers,data=payload) as response:
            response_json = await response.json()
            if response_json['insertedId'] != None:
                self.video = True
                self.oid = "https://ogp-embed-builder-1.herokuapp.com/?oid=" + response_json['insertedId']
                await self.session.close()
        
    def add_author(self,name=None, url=None, icon_url=None,proxy_url=None):
        author_structure = {}

        if name:
            author_structure['name'] = name
        
        if url:
            author_structure['url'] = url

        if icon_url:
            author_structure['icon_url'] = icon_url

        if proxy_url:
            author_structure['proxy_url'] = proxy_url

        self.embed_json['author'] = author_structure

    def add_field(self,name,value,inline=False):
        field_structure = {
            'name':name,
            'value':value,
            'inline':inline
        }
        self.embed_json['fields'].append(field_structure)


class Components():

    def __init__(self):
        self.structure = []
    
    def custom_id(self):
        random_id = ''.join((random.choice(string.hexdigits) for x in range(64)))
        return random_id

    def create_action_row(self):
        if len(self.structure) <= 5:
            action_row_data = {
                "type": 1,
                "components": []
            }
            self.structure.append(action_row_data)
        return self.structure[-1]
    
    def create_button(self,action_row,style,label=None,emoji=None,custom_id=None,link=None,disabled=False):
        if style.lower() == "primary":
            style_value = 1

        elif style.lower() == "secondary":
            style_value = 2
        
        elif style.lower() == "success":
            style_value = 3

        elif style.lower() == "danger":
            style_value = 4
        
        elif style.lower() == "link":
            style_value = 5

        if style_value < 5 and custom_id == None:
            custom_id = self.custom_id()
            required = "custom_id"
        if style_value == 5 and (link == None or (link.startswith('https://') == False)):
            raise "Link Button Must have a valid URL"
        
        button_structure = {}


        