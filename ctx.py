API_ENDPOINT = 'https://discord.com/api/v9'

class context_object():
    class author_object():
        def __init__(self,message_data):
            author = message_data['author']
            self.author = author['username'] + '#' + author['discriminator']
            self.username = author['username']
            self.discriminator = author['discriminator']
            self.id = author['id']
            self.avatar = f"https://cdn.discordapp.com/avatars/{self.id}/{author['avatar']}.png"
            self.avatar_id = author['avatar']
            self.member = message_data['member']

    class message_object():
        def __init__(self,message_data,author_deatails):
            self.content = message_data['content']
            self.attachment = message_data['attachments']
            self.timestamp = message_data['timestamp']
            self.mentions = message_data['mentions']
            self.role_mentions = message_data['mention_roles']
            self.mention_everyone = message_data['mention_everyone']
            self.tts = message_data['tts']
            self.id = message_data['id']
            self.channel_id = message_data['channel_id']
            self.author = author_deatails
            self.guild_id = message_data['guild_id']
            self.components = message_data['components']

    def __init__(self, bot, message_data):
        self.bot = bot
        self.author = self.author_object(message_data)
        self.message = self.message_object(message_data,self.author)
        self.raw_data = message_data

    async def send(self,content=None,embed=None,**kwargs):
        message_data = {}
        headers = {}
        headers['Authorization'] = 'Bot ' + self.bot.token
        if content:
            message_data['content'] = content
        if embed:
            message_data['embed'] = embed.embed_json
        async with self.bot.session.post(url=API_ENDPOINT+f"/channels/{self.message.channel_id}/messages",json=message_data,headers=headers) as response:
            response_text = await response.text()
            print(response_text)
        
        if embed.video:
            extra = {}
            extra['content'] = embed.oid
            async with self.bot.session.post(url=API_ENDPOINT+f"/channels/{self.message.channel_id}/messages",json=extra,headers=headers) as response:
                response_text = await response.text()
                print(response_text)

            
