from datetime import datetime
import aiohttp
import asyncio
import zlib
import json
import traceback
import ctx

class Client:
    def __init__(self, token=None, prefix=None):
        self.loop = None
        self.token = token
        self.session = None
        self._decompressor = zlib.decompressobj()
        self._compressor = zlib.compressobj()
        self.heartbeat_sent = False
        self.ws = None
        self.authentication = False
        self.heartbeat_handshake = False
        self._heartbeat_interval = 20
        self.prefix = prefix
        self.message_commands = {}

    def session_creator(self):
        try:
            if self.session.closed:
                self.session = aiohttp.ClientSession()
        except:
            self.session = aiohttp.ClientSession()

    def message_command(self, *args, **kwargs):
        def decorator(function):
            name = kwargs['name']
            if name == None:
                name = function.__name__
            self.message_commands[name] = function
        return decorator

    async def function_called(self,json_data, prefix_used):
        context = ctx.context_object(bot=self, message_data=json_data['d'])
        name = str(context.message.content).lstrip(prefix_used).split()[0]
        if self.message_commands[name] != None:
            called_function = self.message_commands[name]
            print(called_function)
            await called_function(context)
        else:
            raise f"No command called {name}"

    async def response_sorter(self,raw_ws_data):
        if raw_ws_data == None:
            return await self.websocket_listener()
        data_bytes = self._decompressor.decompress(raw_ws_data.data)
        json_data = json.loads(data_bytes.decode())
        op_cipher = {
            0:{
                'name':'Dispatch',
                'client_action':'Receive',
                'description':'An event was dispatched.'
                },
            1:{
                'name':'Heartbeat',
                'client_action':'Receive',
                'description':'Fired periodically by the client to keep the connection alive.'
                },
            2:{
                'name':'Identify',
                'client_action':'Send',
                'description':'Starts a new session during the initial handshake.'
                },
            3:{
                'name':'Presence Update',
                'client_action':'Send',
                'description':"Update the client's presence."
                },
            4:{
                'name':'Voice State Update',
                'client_action':'Send',
                'description':"Used to join/leave or move between voice channels."
                },
            6:{
                'name':'Resume',
                'client_action':'Send',
                'description':"Resume a previous session that was disconnected."
                },
            7:{
                'name':'Reconnect',
                'client_action':'Receive',
                'description':"You should attempt to reconnect and resume immediately."
                },
            8:{
                'name':'Request Guild Members',
                'client_action':'Send',
                'description':"Request information about offline guild members in a large guild."
            },
            9:{
                'name':'Invalid Session',
                'client_action':'Receive',
                'description':"The session has been invalidated. You should reconnect and identify/resume accordingly."
                },
            10:{
                'name':'Hello',
                'client_action':'Receive',
                'description':"Sent immediately after connecting, contains the heartbeat_interval to use."
                },
            11:{
                'name':'Heartbeat ACK',
                'client_action':'Receive',
                'description':"Sent in response to receiving a heartbeat to acknowledge that it has been received."
            }
        }

        op_code = json_data['op']
        self.last_code = op_code
        if op_cipher[op_code]['client_action'] == "Receive":
            if op_code == 0:
                action = json_data['t']
                if action == "READY":
                    await self.client_info(json_data)
                elif action == "MESSAGE_CREATE":
                    valid, used_prefix = await self.prefix_validate(json_data)
                    if valid:
                        try:
                            await self.function_called(json_data,used_prefix)
                        except Exception as e:
                            print(e)
                            traceback.print_exc()
                
            elif op_code == 1:
                await self.send_heartbeat()
            elif op_code == 7:
                pass
            elif op_code == 9:
                self.authentication = False
            elif op_code == 10:
                self._heartbeat_interval = ((json_data['d'].get('heartbeat_interval'))/1000)*.5
                await self.send_heartbeat()
            elif op_code == 11:
                self.heartbeat_handshake = True
                if self.authentication == False:
                    await self.authenticate()
        print(json_data)
        return await self.websocket_listener()

    async def send_heartbeat(self):
        self._heartbeat_data = {"op": 1,"d": 251}
        await self.ws.send_json(self._heartbeat_data)
        self._last_heartbeat = datetime.now()

    async def client_info(self,json_data):
        self.session_id = json_data['d']['session_id']
        self.discriminator = json_data['d']['user']['discriminator']
        self.username = json_data['d']['user']['username']
        self.user_id = json_data['d']['user']['id']
        self.is_bot = json_data['d']['user']['bot']
        self.authentication = True

    async def prefix_validate(self,json_data):
        message_content = json_data['d']['content']
        for prefix in self.prefix:
            if message_content.startswith(prefix):
                return (True,prefix)
        return (False,None)

    async def authenticate(self):
        auth_data ={"op": 2,"d": {"token": self.token,
            "properties": {"$os": "windows","$browser": "disco","$device": "disco"},
            "intents": 513}}
        await self.ws.send_json(auth_data)

    async def socket_connect(self):
        self.session_creator()
        URL = "wss://gateway.discord.gg?encoding=json&v=9&compress=zlib-stream"
        self.ws = await self.session.ws_connect(URL)
        return await self.websocket_listener()
        
    async def websocket_listener(self):
        if self.heartbeat_handshake and (self._heartbeat_interval < datetime.now().timestamp() - self._last_heartbeat.timestamp()):
            await self.send_heartbeat()
        try:
            raw_data = await asyncio.wait_for(self.ws.receive(), timeout=self._heartbeat_interval)
        except asyncio.exceptions.TimeoutError:
            print(self.last_code)
            raw_data = None
        return await self.response_sorter(raw_ws_data=raw_data)            

    def start(self):
        self.loop = asyncio.new_event_loop()
        self.loop.create_task(asyncio.run(self.socket_connect()))
        self.loop.run_until_complete()
